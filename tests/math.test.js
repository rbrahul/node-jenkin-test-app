const {add,subtract}= require("../lib/math")


test("Test if add returns expected result", () => {
    expect(add(4,5)).toBe(9)
});

test("Test if substract returns expected result", () => {
    expect(subtract(9,3)).toBe(6)
});